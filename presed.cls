% Copyright CNRS/Inria/UNS
% Contributor(s): Eric Debreuve (since 2017)
%
% eric.debreuve@cnrs.fr
%
% This software is governed by the CeCILL  license under French law and
% abiding by the rules of distribution of free software.  You can  use,
% modify and/ or redistribute the software under the terms of the CeCILL
% license as circulated by CEA, CNRS and INRIA at the following URL
% "http://www.cecill.info".
%
% As a counterpart to the access to the source code and  rights to copy,
% modify and redistribute granted by the license, users are provided only
% with a limited warranty  and the software's author,  the holder of the
% economic rights,  and the successive licensors  have only  limited
% liability.
%
% In this respect, the user's attention is drawn to the risks associated
% with loading,  using,  modifying and/or developing or reproducing the
% software by the user in light of its specific status of free software,
% that may mean  that it is complicated to manipulate,  and  that  also
% therefore means  that it is reserved for developers  and  experienced
% professionals having in-depth computer knowledge. Users are therefore
% encouraged to load and test the software's suitability as regards their
% requirements in conditions enabling the security of their systems and/or
% data to be ensured and,  more generally, to use and operate it in the
% same conditions as regards security.
%
% The fact that you are presently reading this means that you have had
% knowledge of the CeCILL license and that you accept its terms.

% presed = PresED = Presentation class by Eric D.
\NeedsTeXFormat {LaTeX2e}[1997/06/01]
\ProvidesClass  {presed} [2017/10/01 v2017.10.01 PresED Class]
\LoadClass[20pt]{extarticle}% /!\ Using points here instead of cm



% TABLE of CONTENTS
%
% CONSTANTS:         colors, margins...
% PROGRAMMING:       arithemtic, control structures
% DEBUGGING:         show page layout
% SLIDE GEOMETRY:    sizes, margins... (relies on package geometry)
% SLIDE DECORATIONS: header, footers, page number... (relies on package fancyhdr)
% SLIDE:             main environment
% SLIDE LAYOUT:      mainly textpos package configuration and tab marks
% SLIDE CONTENTS     fonts, lists, math...
% PRESENTER NOTES:   presenter note command
% CLASS OPTIONS:     draft, dbframe, dblayout, esogrid, textpos+tikz-based grid, pnotes
% OPENING & CLOSING COMMANDS: global counters
%
% Each section can have the following subsections:
% --- Internals
% --- Packages
% --- Configuration
% --- Private Commands
% --- Exported Commands



% EXPORTED COMMANDS
% Exported commands follow the format \presedThisIsAnExportedCommand. In contrast,
% private elements have a prefix presed@.
%
%     /bin/grep newcommand presed* | \
%         /bin/grep -v '^[ ]*%' | \
%         cut -d '{' -f 2 | \
%         cut -d '}' -f 1 | \
%         sort -u | \
%         /bin/grep -v @
%
% \bi   \be   \bd
% \bii  \bee  \bdd
% \biii \beee \bddd
% \biv  \bev  \bdv
%
% \caution
% \goto
% \impliestgl
% \inlinerem
% \isparentof
% \normalcfs
% \trademark
%
% \presedCaption
% \presedCenterContents
% \presedCenterContentsV
% \presedCloseAllLists
% \presedFirstImg
% \presedNextImg
% \presedLastImg
% \presedHyperlinkToPage
% \presedImageWithOverlayedCaption
% \presedNewRowForCaptions
% \presedNewRowForImages
% \presedNewRowForSplittedCaption
% \presednl
% \presedOutlineMark
% \presedPageHyperLabel
% \presedPNote
% \presedSeeSlideRef
% \presedSetPageFormat
% \presedSetPageNumberPrefix
% \presedSetPart
% \presedSetSlideTitle
% \presedSlide
% \presedSlideRef
% \presedTitleAndAuthors
% \presedBuildFooter
% \presedWithItemIStyle



% CONSTANTS
%
\newcommand*{\presed@UnitlessContentWidth} {33}
\newcommand*{\presed@UnitlessContentHeight}{22}

\newcommand*{\presed@PartColor}   {cyan}
\newcommand*{\presed@TitleColor}  {blue}
\newcommand*{\presed@ItemIColor}  {teal}
\newcommand*{\presed@CaptionColor}{violet}

\newcommand*{\presed@HeaderRuleLeftColor} {blue}
\newcommand*{\presed@HeaderRuleRightColor}{rgb,255:red,70;green,195;blue,245}
\newcommand*{\presed@FooterRuleColor}     {gray}



% PROGRAMMING
%
\RequirePackage{calc}
\RequirePackage{forloop}
\RequirePackage{ifthen}
\RequirePackage{xargs}
\RequirePackage{xstring}
%xargs: \newcommandx<∗>{<command>}[<number>][<list>]{<definition>}
%       The optional ∗ makes LaTeX define a "short" macro, that is a macro
%       that won’t accept a paragraph break (\par or an empty line) inside its argument.



% DEBUGGING
%
\newboolean{presed@WithFrame} \setboolean{presed@WithFrame} {false}
\newboolean{presed@WithLayout}\setboolean{presed@WithLayout}{false}



% SLIDE GEOMETRY
%
% Width x height = 36cm x 27cm
% Horizontal margin           = 1.5cm   => Content area width  = 33cm
% Top/Bottom vertical margins = 3cm/2cm => Content area height = 22cm
%     Aspect ratio = (9 * 4) / (9 * 3) = 4/3
%     Width  = horizontal   margin + content area width  + horizontal      margin
%     Height = top vertical margin + content area height + bottom vertical margin
%
% --- Internals
%
\newboolean{presed@VariableHeightMode}\setboolean{presed@VariableHeightMode}{false}

\newlength{\presed@PaperWidth}  \newlength{\presed@ContentWidth}
\newlength{\presed@PaperHeight} \newlength{\presed@ContentHeight}
\newlength{\presed@HeaderHeight}\newlength{\presed@FooterSkip}
\newlength{\presed@UniversalMargin}
\newlength{\presed@HorizontalMargin}
\newlength{\presed@TopVerticalMargin}
\newlength{\presed@BotVerticalMargin}
\newlength{\presed@TopVerticalVarHeightMargin}
\newlength{\presed@BotVerticalVarHeightMargin}
\newlength{\presed@FootRuleSkip}

\setlength{\presed@HorizontalMargin} {1.5cm}
\setlength{\presed@TopVerticalMargin}{3cm}
\setlength{\presed@BotVerticalMargin}{2cm}

\setlength{\presed@UniversalMargin}{0.5cm}
\setlength{\presed@HeaderHeight}{\presed@TopVerticalMargin - \presed@UniversalMargin}
\setlength{\presed@FooterSkip}  {\presed@BotVerticalMargin - \presed@UniversalMargin}
\setlength{\presed@FootRuleSkip}{2\presed@UniversalMargin + 0.35\footskip - \presed@BotVerticalMargin}

\setlength{\presed@ContentWidth} {\presed@UnitlessContentWidth  cm}
\setlength{\presed@ContentHeight}{\presed@UnitlessContentHeight cm}
\setlength{\presed@PaperWidth}   {\presed@ContentWidth  + \presed@HorizontalMargin * 2}
\setlength{\presed@PaperHeight}  {\presed@ContentHeight + \presed@TopVerticalMargin +%
                                                          \presed@BotVerticalMargin}
%
% --- Packages
%
\RequirePackage[papersize={\presed@PaperWidth,\presed@PaperHeight},%
                total={\presed@ContentWidth,\presed@ContentHeight},%
                left=\presed@HorizontalMargin,top=\presed@TopVerticalMargin,%
                headheight=\presed@HeaderHeight,headsep=\presed@UniversalMargin,%
                footskip=\presed@FooterSkip]{geometry}



% SLIDE DECORATIONS
%
% \lfoot and \cfoot can be used freely
% (at default font size if desired, i.o.w., no requirement to tweak font size)
% outline90.ong: http://fontmeme.com/outline-fonts/ Font:BASIF___, Size:65, Color:#0202BB
%
% --- Internals
%
\newcounter{presed@OutlineTopRightHPos}
\setcounter{presed@OutlineTopRightHPos}{\presed@UnitlessContentWidth - 5}

\newcommand*{\presed@PageNumber}{\presed@PageNumberStyle\presed@PageNumberPrefix\thepage}% Default page format
\newcommand*{\presed@PageNumberStyle}{\small\tt}
\newcommand*{\presed@PageNumberPrefix}{\#}

\newcommand*{\presed@OutlineImgPath}{\presed@InstallationPath/presed_outline90}
%
% --- Packages
%
\RequirePackage{fancyhdr}
\RequirePackage{tikz}
%
% --- Configuration
%
\setcounter{page}{0}% Here instead of AtBeginDocument so that it can be overridden easily in document preamble
\pagestyle{fancy}
\fancyhf{}
\renewcommand{\footruleskip}{\presed@FootRuleSkip}
\rfoot{\presed@RightFooter}
%
% --- Private Commands
%
\newcommand*{\presed@FormattedPresPart}    [1]{\textcolor{\presed@PartColor} {\large\it#1}}
\newcommand*{\presed@FormattedSlideTitle}  [1]{\textcolor{\presed@TitleColor}{\Large\bf#1}}
\newcommand*{\presed@FormattedLeftFooter}  [2]{\textcolor{gray}{\it\small#1~/\href{mailto:#2}{#2}/}}
\newcommand*{\presed@FormattedMiddleFooter}[1]{\textcolor{gray}{\it\small#1}}
\newcommand*{\presed@LeftFooter}{}
\newcommand*{\presed@MiddleFooter}{}
\newcommand*{\presed@RightFooter}{\textcolor[\presed@FooterRuleColor]{0.6}{\presed@PageNumber}}

\renewcommand{\headrule}{%
    \tikz[remember picture, overlay]{%
        \fill[left  color=\presed@HeaderRuleLeftColor, right color=\presed@HeaderRuleRightColor]
        (0,0) rectangle (\textwidth,0.05cm);%
        \fill[color=\presed@HeaderRuleRightColor]
        (\textwidth,0cm) rectangle +(-0.1cm,-0.65cm);}%
}

\renewcommand{\footrule}{%
    \tikz[remember picture, overlay, color=\presed@FooterRuleColor]{%
        \fill (0,0.75cm) rectangle +(\textwidth,0.05cm);%
        \fill (0,0.75cm) rectangle +(0.1cm,0.65cm);}%
}
%
% --- Exported Commands
%
\newcommand{\presedTitleAndAuthors}[2]{
    \presedCenterContents{%
        \Huge#1\\[3cm]
        \Large#2}%
}

\newcommand*{\presedSetPageFormat}[1]{%
    \renewcommand*{\presed@PageNumber}{#1}%
}
\newcommand*{\presedSetPageNumberPrefix}[1]{%
    \renewcommand*{\presed@PageNumberPrefix}%
    {#1\rule[0.02cm]{0.4cm}{0.05cm}\hspace{0.02cm}}%
}
\newcommand*{\presedBuildFooter}[3]{%
    \ifthenelse{\boolean{presed@VariableHeightMode}}
        {%
            \renewcommand*{\presed@LeftFooter}{\presed@FormattedLeftFooter{#1}{#3}}%
            \renewcommand*{\presed@MiddleFooter}{\presed@FormattedMiddleFooter{#2}}%
        }
        {%
            \lfoot{\presed@FormattedLeftFooter{#1}{#3}}%
            \cfoot{\presed@FormattedMiddleFooter{#2}}%
        }%
}

\newcommand*{\presedOutlineMark}{%
    \begin{textblock}{5}[1,0](\value{presed@OutlineTopRightHPos},1)
                      % [1,0]=top-right corner
                      % left-->right: \presed@UnitlessContentWidth - 5 ("same 5" appears in "{5}")
                      % top-->bottom: 1 (2cm below = 2 * "1 here")
        \centering%
        \includegraphics[height=\presed@ContentHeight - 2cm]{\presed@OutlineImgPath}%
    \end{textblock}%
}

\newcommand*{\presedSlideRef}   [1]{\presed@PageNumberPrefix\pageref{#1}}
\newcommand*{\presedSeeSlideRef}[1]{{\small(see \presedSlideRef{#1})}}



% SLIDE
%
% --- Internals
%
\newboolean{presed@IsFirstSlide}

\newenvironment{presed@SlideEnv}[2]{%
    \ifthenelse{\boolean{presed@VariableHeightMode}}
        {%
            \begin{preview}%
                \vspace*{\presed@TopVerticalVarHeightMargin}%
                \centerline{\presed@FormattedSlideTitle{#1}\hfill\presed@FormattedPresPart{#2}}%
                \headrule%
        }
        {}%
    \presed@DrawGrid%
    \ifthenelse{\equal{#2}{}}
        {\presedSetPart{}}
        {\presedSetPart{#2}}%
    \presedSetSlideTitle{#1}%
    \onecolumn%
}{%
    \presed@EnsureListLevelIs{}{0}%
    \ifthenelse{\boolean{presed@VariableHeightMode}}
        {%
                \vspace{0.75cm}%
                \footrule%
                \makebox[0pt][l]{\presed@LeftFooter}\hfill%
                \makebox[0pt][c]{\presed@MiddleFooter}\hfill%
                \makebox[0pt][r]{\presed@RightFooter}%
                \vspace*{\presed@BotVerticalVarHeightMargin}%
            \end{preview}%
            \stepcounter{page}%
        }
        {\newpage}%
}
%
% --- Exported Commands
%
\newcommand*{\presedSetPart}      [1]{\rhead{\presed@FormattedPresPart{#1}}}
\newcommand*{\presedSetSlideTitle}[1]{\lhead{\presed@FormattedSlideTitle{#1}}}

\newcommandx*{\presedSlide}[2][2=]{%
    % title[, part (right-justified)]
    \ifthenelse{\boolean{presed@IsFirstSlide}}
        {\setboolean{presed@IsFirstSlide}{false}}
        {\end{presed@SlideEnv}}%
    %
    \begin{presed@SlideEnv}{#1}{#2}
}

\newcommand{\presedCenterContentsV}[1]{%
    \nointerlineskip%
    \vspace*{\fill}%
    #1%
    \vspace*{\fill}%
}

\newcommand{\presedCenterContents}[1]{%
    \presedCenterContentsV{%
        \centering%
        #1%
    }%
}



% SLIDE LAYOUT
%
% --- Internals
%
\newboolean{presed@WithEsoGrid}\setboolean{presed@WithEsoGrid}{false}
\newlength{\presed@LineIdentation}
\newlength{\presed@FixedPosition}
\newcommand*{\presed@DrawGrid}{}
%
% --- Packages
%
\RequirePackage{multicol}
\RequirePackage{tabto}
\RequirePackage[absolute,overlay]{textpos}% To position something anywhere
%
% --- Configuration
%
\setlength{\parindent}{0cm}
\setlength{\multicolsep}{\baselineskip}
\setlength{\columnsep}{1.5cm}

\TPGrid[\presed@HorizontalMargin,\presed@TopVerticalMargin]%
    {\presed@UnitlessContentWidth}%
    {\presed@UnitlessContentHeight}

\renewcommandx*{\section}[2]%
    [2=gray]%
    {%
        \presed@EnsureListLevelIs{}{0}%
        \textcolor{#2}{\large\sc#1\dotfill\newline}%
    }
%
% --- Exported Commands
%
\newcommandx*{\presednl}[2]% #1=vertical skip, #2=any state modifiers
    [1=\itemsep, 2=]%
    {%
        \ifthenelse{\equal{#2}{}} {\normalcfs} {#2}%
        \hfill\vspace*{#1}\newline%
    }

\newcommand*{\goto}[1]{%
    \setlength{\presed@FixedPosition}{#1cm}%
    \addtolength{\presed@FixedPosition}{-\presed@LineIdentation}%
    \tabto*{\presed@FixedPosition}%
}



% SLIDE CONTENTS
%
% Relative inputs refer to the folder of the main LaTeX document, not the
% folder of the currently processed document. It is therefore necessary to
% retrieve the later in order to input files using absolute paths. Note that,
% if placed too early in the file, the code below hangs, maybe because the
% contents of the LOG file has not started to be written to disk yet. The
% below procedure might be risky then (or is there a way to flush the LOG
% file buffer?).
\newcommand*{\presed@ClassName}{presed}
\newread\presed@MainDocumentLOG
\immediate\openin\presed@MainDocumentLOG=\jobname.log
\newcounter{presed@ShouldStopLooping}
\setcounter{presed@ShouldStopLooping}{0}
\loop
    \immediate\read\presed@MainDocumentLOG to \presed@LOGLine
    % \ifeof\presed@MainDocumentLOG
    %     \setcounter{presed@ShouldStopLooping}{1}
    % \else
        \expandarg
        \IfSubStr{\presed@LOGLine}{\presed@ClassName}
            {\setcounter{presed@ShouldStopLooping}{1}}
            {}
    % \fi
\ifnum\value{presed@ShouldStopLooping}>0\else\repeat
\immediate\closein\presed@MainDocumentLOG
\expandarg\StrBehind{\presed@LOGLine}{(}[\presed@FileFullPath]
\expandarg\StrBefore{\presed@FileFullPath}{\presed@ClassName.cls}[\presed@InstallationPath]

\input{\presed@InstallationPath/presed_font}
\input{\presed@InstallationPath/presed_text}
\input{\presed@InstallationPath/presed_symbol}
\input{\presed@InstallationPath/presed_list}
\input{\presed@InstallationPath/presed_math}
\input{\presed@InstallationPath/presed_graphics}
\input{\presed@InstallationPath/presed_xref}



% PRESENTER NOTES
%
\newcommandx{\presedPNote}[2][2=1]{}



% CLASS OPTIONS
%
\DeclareOption{draft}{%
    \setkeys{Gin}{draft}%
}

\DeclareOption{dbframe}{%
    \setboolean{presed@WithFrame}{true}%
}

\DeclareOption{dblayout}{%
    \setboolean{presed@WithLayout}{true}%
}

\DeclareOption{esogrid}{%
    \setboolean{presed@WithEsoGrid}{true}%
}

\DeclareOption{grid}{%
    \renewcommand*{\presed@DrawGrid}{%
        \begin{textblock}{\presed@UnitlessContentWidth}(0,0)
            \tikz{%
                \draw[color=lightgray]
                    (0,1) grid (\presed@UnitlessContentWidth,\presed@UnitlessContentHeight + 1);
                \foreach \x in {1,...,\presed@UnitlessContentWidth}
                    \node[color=orange, anchor=north] at (\x cm, 1.5) {\x};
                \foreach \y in {1,...,\presed@UnitlessContentHeight} {%
                    \pgfmathtruncatemacro\reversedy{\presed@UnitlessContentHeight - \y + 1}%
                    \node[color=olive, anchor=east]   at (1.2, \y cm) {\reversedy};
                }%
            }%
        \end{textblock}%
    }%
}

\DeclareOption{varheight}{%
    \setboolean{presed@VariableHeightMode}{true}%
    \renewcommand{\presedCenterContentsV}[1]{#1\par}
}

\DeclareOption{pnotes}{%
    \setlength{\paperheight}{\paperheight * 2}
    %
    \newlength{\presed@vPosIncrement}
    \newlength{\presed@PNoteVPos}
    \setlength{\presed@PNoteVPos}{\textheight + \footskip + 1cm}
    %
    \newcounter{presed@PNote}\setcounter{presed@PNote}{1}
    \newsavebox{\presed@SavedPNoteMark}
    %
    \renewcommandx{\presedPNote}[2]%
        [2=1]%
        {%
            \savebox{\presed@SavedPNoteMark}{$^\mathrm{\textcolor{red}{|\arabic{presed@PNote}}}$}%
            \makebox[0cm][l]{\usebox{\presed@SavedPNoteMark}}%
            \vadjust{% See below
                \begin{textblock*}{\textwidth}(0cm,\presed@PNoteVPos)%
                    \usebox{\presed@SavedPNoteMark}\textcolor{red}{\normalfont\footnotesize#1}%
                \end{textblock*}%
            }%
            \setlength{\presed@vPosIncrement}{\baselineskip * #2 + 0.25cm}
            \global\advance\presed@PNoteVPos by \presed@vPosIncrement% \global\addtolength does not work here (see below)
            \refstepcounter{presed@PNote}%
        }
}

\DeclareOption*{% unknown option
    \PackageWarningNoLine{presed}{Unknown option "\CurrentOption".\MessageBreak%
    Valid options: "draft", "dbframe", "dblayout", "esogrid", "grid", "pnotes".\MessageBreak%
    Default: None.%
    }%
}

\ProcessOptions\relax

\ifthenelse{\boolean{presed@VariableHeightMode}}
    {%
        \RequirePackage[active,pdftex,tightpage]{preview}%
        \renewcommand{\PreviewBorder}{\presed@HorizontalMargin}%
        \setlength{\presed@TopVerticalVarHeightMargin}{1.5\presed@UniversalMargin - \PreviewBorder}%
        \setlength{\presed@BotVerticalVarHeightMargin}{\presed@UniversalMargin - \PreviewBorder}%
    }
    {}
\ifthenelse{\boolean{presed@WithFrame}}
    {\RequirePackage{showframe}}
    {}
\ifthenelse{\boolean{presed@WithLayout}}
    {\RequirePackage{layout}}
    {}
\ifthenelse{\boolean{presed@WithEsoGrid}}
    {\RequirePackage[texcoord,grid,gridunit=mm,%
                     gridcolor=red!10,subgridcolor=green!10]%
                    {eso-pic}}
    {}



% OPENING & CLOSING COMMANDS
%
\AtBeginDocument{%
    \setboolean{presed@IsFirstSlide}    {true}%
    \setcounter{presed@CurrentItemLevel}{0}%
    \setlength{\presed@LineIdentation}  {0cm}%
    %
    \ifthenelse{\boolean{presed@WithLayout}}
        {\layout}
        {}%
}

\AtEndDocument{%
    \end{presed@SlideEnv}%
}

% The {textblock} environment will most often be used in vertical mode.
% If it is called in horizontal (ie, paragraph) mode, however, it will
% silently create a paragraph break by inserting a \par command before
% the environment; it remains in vertical mode after the environment is finished.
%
% From: http://tex.stackexchange.com/questions/276433/how-to-prevent-textpos-textblock-environment-from-inserting-par
% By: David Carlisle on Nov 3 '15 at 21:50
%     [\vadjust{...}] doesn't stop it issuing \par but it stops that \par ending the current paragraph.

% Unlike LaTeX counters length registers are group-safe, i.e. changing
% their values inside a group (like \begin{enumerate}...\end{enumerate})
% does not survive the exit of the group, i.e. the change is not
% persistent unless \global\addtolength is used here.
% [...]
% However, since calc changes the definitions of \addtolength
% [...]
% \newcommand{\increaselength}[2]{%
%     \global\advance#1 by #2\relax
% }%
%
% From: http://tex.stackexchange.com/questions/318302/disappearing-custom-length-in-enumitem/318314
% By: Christian Hupfer on Jul 6 '16 at 18:10
