% Copyright CNRS/Inria/UNS
% Contributor(s): Eric Debreuve (since 2017)
%
% eric.debreuve@cnrs.fr
%
% This software is governed by the CeCILL  license under French law and
% abiding by the rules of distribution of free software.  You can  use,
% modify and/ or redistribute the software under the terms of the CeCILL
% license as circulated by CEA, CNRS and INRIA at the following URL
% "http://www.cecill.info".
%
% As a counterpart to the access to the source code and  rights to copy,
% modify and redistribute granted by the license, users are provided only
% with a limited warranty  and the software's author,  the holder of the
% economic rights,  and the successive licensors  have only  limited
% liability.
%
% In this respect, the user's attention is drawn to the risks associated
% with loading,  using,  modifying and/or developing or reproducing the
% software by the user in light of its specific status of free software,
% that may mean  that it is complicated to manipulate,  and  that  also
% therefore means  that it is reserved for developers  and  experienced
% professionals having in-depth computer knowledge. Users are therefore
% encouraged to load and test the software's suitability as regards their
% requirements in conditions enabling the security of their systems and/or
% data to be ensured and,  more generally, to use and operate it in the
% same conditions as regards security.
%
% The fact that you are presently reading this means that you have had
% knowledge of the CeCILL license and that you accept its terms.

% --- Internals
%
\newcounter{presed@NColInTabular}
\newcounter{presed@ColIdxInTabular}

\newlength{\presed@ImgWidthInTabular}
\newlength{\presed@ImgHeightInTabular}
\newlength{\presed@ColSepInTabular}

\newlength{\presed@RowGapForImages}          \setlength{\presed@RowGapForImages}          {0.25cm}
\newlength{\presed@RowGapForCaptions}        \setlength{\presed@RowGapForCaptions}        {-0.6cm}
\newlength{\presed@RowGapForSplittedCaptions}\setlength{\presed@RowGapForSplittedCaptions}{-0.3cm}

\global\def\presed@RowOfImgCaptions{}



% --- Packages
%
%\RequirePackage[pdftex]{graphicx}% Also rotatebox
    % /!\ Already loaded, but by which package?
    % and w/o pdftex option on top of that



% --- Private Commands
%
\newcommand{\presed@PrintImaget}[4]{%
    % image, width (mandatory), height (optional), caption
    \parbox{#2}{%
        \makebox[#2][l]{\hspace*{\fill}\presedCaption{#4}\hspace*{\fill}}%
        \vspace{0.2cm}%
        \mbox{%
            \ifthenelse{\equal{#3}{}}
                {\includegraphics[width=#2]{#1}}
                {\includegraphics[width=#2,height=#3]{#1}}}%
    }%
}

\newcommand{\presed@PrintImager}[4]{%
    % image, width (optional), height (mandatory), caption
    \mbox{%
        \ifthenelse{\equal{#2}{}}
            {\includegraphics[height=#3]{#1}}
            {\includegraphics[height=#3,width=#2]{#1}}%
        \hspace{0.2cm}%
        \rotatebox{90}{\parbox[t]{#3}{\hspace*{\fill}\presedCaption{#4}\hspace*{\fill}}}%
    }%
}

\newcommand{\presed@PrintImageb}[4]{%
    % image, width (mandatory), height (optional), caption
    \parbox{#2}{%
        \mbox{%
            \ifthenelse{\equal{#3}{}}
                {\includegraphics[width=#2]{#1}}
                {\includegraphics[width=#2,height=#3]{#1}}}%
        \newline%
        %\vspace{0.2cm}%
        \makebox[#2][l]{\hspace*{\fill}\presedCaption{#4}\hspace*{\fill}}%
    }%
}

\newcommand{\presed@PrintImagel}[4]{%
    % image, width (optional), height (mandatory), caption
    \mbox{%
        \rotatebox{90}{\parbox[t]{#3}{\hspace*{\fill}\presedCaption{#4}\hspace*{\fill}}}%
        \hspace{0.1cm}%
        \ifthenelse{\equal{#2}{}}
            {\includegraphics[height=#3]{#1}}
            {\includegraphics[height=#3,width=#2]{#1}}%
    }%
}



% --- Exported Commands
%
\newcommandx{\presedImageWithOverlayedCaption}[5]%
    [2=,3=,4=,5=b,usedefault]{%
    % image, width=#2, height=#3, caption=#4, caption position=#5 (t,r,b,l)
    \csname presed@PrintImage#5\endcsname{#1}{#2}{#3}{#4}%
}

\newcommandx{\presedCaption}[2]%
    [2=\presed@CaptionColor]%
    {\textcolor{#2}{\small\it#1}}

\newcommandx{\presedFirstImg}[6]%
    [2=0cm,3=0cm,4=1cm,6=]%
    % n_image_cols, width=#2, height=#3, sep=#4, image, caption=#6
    {%
        \setcounter{presed@NColInTabular}{#1}%
        \setcounter{presed@ColIdxInTabular}{0}%
        %
        \setlength{\presed@ImgWidthInTabular} {#2}%
        \setlength{\presed@ImgHeightInTabular}{#3}%
        \setlength{\presed@ColSepInTabular}   {#4}%
        \setlength{\presed@ColSepInTabular}   {0.5\presed@ColSepInTabular}%
        %
        \begin{tabular}{@{\hspace*{-\presed@ColSepInTabular}}%
                        *{#1}{@{\hspace*{\presed@ColSepInTabular}}c@{\hspace*{\presed@ColSepInTabular}}}%
                        @{\hspace*{-\presed@ColSepInTabular}}}%
            \ifthenelse{\equal{#6}{}}%
                {\presedNextImg{#5}[]}%
                {\presedNextImg{#5}[#6]}%
    }

\newcommandx{\presedNextImg}[3]%
    [2=,3=]%
    % image, caption=#2, last_row=#3 ("lastrow" for true; anything else for false)
    % Not to be called with an explicit last_row argument. This responsability is left to \presedLastImg.
    {%
        \ifthenelse{\value{presed@ColIdxInTabular} > 0}%
            {&\g@addto@macro\presed@RowOfImgCaptions{&}}%
            {}%
        %
        \ifdim\presed@ImgWidthInTabular > 0cm%
            \includegraphics[width=\presed@ImgWidthInTabular]{#1}%
        \else%
            \includegraphics[height=\presed@ImgHeightInTabular]{#1}%
        \fi%
        %
        \ifthenelse{\equal{#2}{}}%
            {}%
            {\g@addto@macro\presed@RowOfImgCaptions{\presedCaption{#2}}}%
        %
        \stepcounter{presed@ColIdxInTabular}%
        %
        \ifthenelse{\value{presed@ColIdxInTabular} < \value{presed@NColInTabular}}%
            {}%
            {%
                \ifthenelse{\equal{\presed@RowOfImgCaptions}{}}%
                    {}%
                    {\\[\presed@RowGapForCaptions]\presed@RowOfImgCaptions}%
                \ifthenelse{\equal{#3}{lastrow}}%
                    {}%
                    {\\[\presed@RowGapForImages]}%
                %
                \setcounter{presed@ColIdxInTabular}{0}%
                \global\def\presed@RowOfImgCaptions{}%
            }%
    }

\newcommandx{\presedLastImg}[2]%
    [2=]%
    % image, caption=#2
    {%
            \ifthenelse{\equal{#2}{}}%
                {\presedNextImg{#1}[][lastrow]}%
                {\presedNextImg{#1}[#2][lastrow]}%
        \end{tabular}%
    }

\newcommand*{\presedNewRowForImages}         {\\[\presed@RowGapForImages]}
\newcommand*{\presedNewRowForCaptions}       {\\[\presed@RowGapForCaptions]}
\newcommand*{\presedNewRowForSplittedCaption}{\\[\presed@RowGapForSplittedCaptions]}
